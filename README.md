# E-commerce Product Inventory Management API

An API that allows an e-commerce platform admin to manage product inventory.

## Key Functionalities:

- Add Product: Add a new product to the database with a name and quantity.
- List Products: Retrieve a list of all products in the database.
- Delete Product: Delete a product from the database using its ID.
- Update Quantity: Increment or decrement the quantity of a specific product using its ID.


## Technologies Employed:

- Node.js: Provides the runtime environment for the web application.
- MongoDB: A NoSQL database used for dependable data storage and retrieval.
- Express: A web application framework for managing HTTP requests and responses.
- Mongoose: A sophisticated MongoDB object modeling tool for database interaction.



## Requirements

For development, you will only need Node.js (16+), a node global package (Npm), and mongoDB atlas URI or locally installed mongodb server.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

  If the installation was successful, you should be able to run the following command.

    $ node --version <br>
    V16.xx.xx

    $ npm --version <br>
    x.xx.xx

  If you need to update `npm`, you can make it using `npm` !Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g


## Install Project

    $ git clone https://gitlab.com/bhagwatijoshi325/ecommerece-apis
    $ cd habit-maker
    $ npm install

## Running the project on development
    # Set up .env file in the root, format is given in the .env.example
    
    $ npm run start:dev


## Folder Structure
```
[Ecommerce Product Inventory Management API]
│   .env
│   .gitignore
│   server.js
│   package-lock.json
│   package.json
│   README.md
│
├───controllers
│       productsController.js
│
├───models
│       productModel.js
│
└───routes
        productsRoutes.js
```


## Support

Contributions, bug reports, and feature requests are welcome. Feel free to fork the repository and submit pull requests to contribute to the project.