const Product = require('../models/productModel');

// Add a new product
exports.createProduct = async (req, res) => {
    try {
        const product = new Product(req.body.product);
        await product.save();
        res.status(201).json({ data: { product } });
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}

// List all Products
exports.listProducts = async (req, res) => {
    try {
        const products = await Product.find();
        res.status(200).json({ data: { products } });
    } catch (error) {
        res.status(400).json({ message: error.message })
    }
}

// Delete a product
exports.deleteProduct = async (req, res) => {
    try {
        await Product.findByIdAndDelete(req.params.id);
        res.status(200).json({ data: { message: 'product deleted' } });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};

// Update the quantity of a product
exports.updateQuantity = async (req, res) => {
    try {
        const product = await Product.findById(req.params.id);
        product.quantity += parseInt(req.query.number, 10);
        await product.save();
        res.status(200).json({ data: { product, message: 'updated successfully' } });
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
};