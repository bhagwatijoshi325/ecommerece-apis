const express = require('express');
const {
    createProduct,
    listProducts,
    deleteProduct,
    updateQuantity
} = require('../controllers/productsController');

const router = express.Router();

router.post('/create' , createProduct);
router.get('/' , listProducts);
router.delete('/:id' , deleteProduct);
router.post('/:id/update_quantity' , updateQuantity);

module.exports = router;

